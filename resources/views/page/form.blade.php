@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    
    <form action="/welcome" method="post">
        @csrf

        <!-- Nama -->
        <label for="fname">First name:</label>
        <br><br>
        <input type="text"  name="fname">
        <br><br>
        <label for="lname">Last name:</label>
        <br><br>
        <input type="text"  name="lname">
        <br>

        <!-- Gender -->
        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label>
        <br><br>

        <!-- Negara -->
        <label for="nationality">Nationality</label>
        <br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Inggris">Inggris</option>
            <option value="Amerika">Amerika</option>
        </select>
        <br><br>

        <!-- Bahasa -->
        <label for="language_spoken">Language Spoken:</label>
        <br><br>
        <input type="checkbox" id="indonesia" name="indonesia" value="Bahasa Indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" id="other" name="other" value="Other">
        <label for="other">Other</label>
        <br><br>

        <!-- Bio -->
        <label for="Bio">Bio:</label>
        <br><br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea>
        <br>

        <input type="submit" value="Sign Up">

    </form>


@endsection


   